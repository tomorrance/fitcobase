# FitcoBase

핏코 iOS App Base Library



## 정리가 끝난 것

- [x] Navigation Bar
- [x] Default Button
- [x] Alert



### 1. Navigation Bar



``` swift
import FitcoBase

lazy var navBar: NavBar = {
    let type = NavBarType(titleType: NavConstant.Default.title,
                          leftType: NavConstant.Default.popButton,
                          rightType: NavConstant.Default.button)
    let view = NavBar(type: type)
    view.leftButton.addTarget(self,
                              action: #selector(popDidTap),
                              for: .touchUpInside)
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()

```





### 2. Default Button



``` swift

import FitcoBase

  lazy var button: CenterButton = {
    let type = CustomType(font: .notoM17,
                          title: "디폴트 버튼임",
                          enabledBackColor: .fITCOGrey800,
                          unabledBackColor: .fITCOGrey300,
                          titleColor: .white)
    let button = CenterButton(type: type)
    button.addTarget(self, action: #selector(buttonDidTap), for: .touchUpInside)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()

```





### 3. Alert



``` swift
import FitcoBase

AlertBlackView.show(message: self.textField.text ?? "", font: .notoM17)

```

