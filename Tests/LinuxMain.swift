import XCTest

import FitcoBaseTests

var tests = [XCTestCaseEntry]()
tests += FitcoBaseTests.allTests()
XCTMain(tests)
