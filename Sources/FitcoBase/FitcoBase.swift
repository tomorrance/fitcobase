import UIKit
import SnapKit

public enum CenterButtonType {
  case `default`
  case greyLine
  case orange
}
public struct CustomButtonType {
  var font: UIFont?
  var image: UIImage?
  var title: String
  var enabledBackColor: UIColor
  var unabledBackColor: UIColor
  var titleColor: UIColor
  var type: CenterButtonType
  var autoWidth: Bool
  
  public init(font: UIFont? = nil,
              image: UIImage?,
              title: String,
              enabledBackColor: UIColor,
              unabledBackColor: UIColor,
              titleColor: UIColor,
              type: CenterButtonType = .default,
              autoWidth: Bool = false) {
    self.autoWidth = autoWidth
    self.font = font
    self.title = title
    self.enabledBackColor = enabledBackColor
    self.unabledBackColor = unabledBackColor
    self.titleColor = titleColor
    self.type = type
    self.image = image
  }
  
}

open class CenterButton: UIButton {
  
  var typeValue: CustomButtonType
  
  public init(type: CustomButtonType) {
    self.typeValue = type
    super.init(frame: .zero)
    
    if let font = type.font {
      self.textLabel.font = font
    }
    self.textLabel.text = type.title
    self.textLabel.textColor = type.titleColor
    
    self.containerView.backgroundColor = typeValue.enabledBackColor
    
    if type.type == .greyLine {
      self.greyLineButton()
    } else if type.type == .orange {
      self.orangeButton()
    }
    
    setData(image: type.image)
    
    
    initView()
  }
  
  
  public override var isEnabled: Bool {
    didSet {
      if isEnabled {
        UIView.animate(withDuration: 0.3) {
          self.containerView.backgroundColor = self.typeValue.enabledBackColor
        }
      } else {
        UIView.animate(withDuration: 0.3) {
          self.containerView.backgroundColor = self.typeValue.unabledBackColor
        }
      }
    }
  }
  
  func orangeButton() {
    customImageView.tintColor = .white
    containerView.backgroundColor = .fITCOOrange500
    containerView.layer.cornerRadius = 12
  }
  
  
  func greyLineButton() {
    customImageView.tintColor = UIColor.fITCOGrey600
    textLabel.textColor = .fITCOGrey600
    containerView.layer.cornerRadius = 12
    containerView.layer.borderWidth = 2
    containerView.layer.borderColor = UIColor.fITCOGrey600.cgColor
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func initView() {
    containerView.isUserInteractionEnabled = false
    self.addSubview(containerView)
    
    containerView.snp.makeConstraints { (make) in
      make.top.leading.trailing.bottom.equalTo(self)
    }
    
  }
  
  private func setData(image: UIImage?) {
    
    
    if let image = image {
      
      customImageView.image = image
      containerView.addSubview(textWithImageWrapper)
      textWithImageWrapper.addSubview(textLabel)
      textWithImageWrapper.addSubview(customImageView)
      
      textWithImageWrapper.snp.makeConstraints { (make) in
        make.top.bottom.equalTo(containerView)
        if typeValue.autoWidth {
          make.leading.equalTo(containerView).offset(24)
          make.trailing.equalTo(containerView).offset(-24)
        } else {
          make.leading.lessThanOrEqualTo(containerView).offset(24).priority(.low)
          make.trailing.lessThanOrEqualTo(containerView).offset(-24).priority(.low)
        }
        make.center.equalTo(containerView)
      }
      customImageView.snp.makeConstraints { (make) in
        make.leading.centerY.equalTo(textWithImageWrapper)
        make.width.height.equalTo(24)
      }
      textLabel.snp.makeConstraints { (make) in
        make.leading.equalTo(customImageView.snp.trailing).offset(8)
        make.top.equalTo(containerView).offset(9)
        make.bottom.equalTo(containerView).offset(-10)
        make.trailing.equalTo(textWithImageWrapper)
        
      }
    } else {
      containerView.addSubview(textLabel)
      
      textLabel.snp.makeConstraints { (make) in
        make.leading.equalTo(containerView).offset(24)
        make.trailing.equalTo(containerView).offset(-24)
        make.top.equalTo(containerView).offset(9)
        make.bottom.equalTo(containerView).offset(-10)
      }
    }
  }
  
  public lazy var containerView: UIView = {
    let view = UIView()
    view.backgroundColor = .fITCOOrange500
    view.layer.cornerRadius = 12
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var textWithImageWrapper: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var customImageView: UIImageView = {
    let view = UIImageView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = .scaleAspectFit
    return view
  }()
  
  public lazy var textLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textColor = .white
    return label
  }()
  
}

