//
//  AlertView.swift
//  FitcoAlert
//
//  Created by Daisy on 2020/03/13.
//  Copyright © 2020 Daisy. All rights reserved.
//
import Foundation
import UIKit

public class AlertBlackView: UIView {
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  public static func show(message: String, font: UIFont) {
    guard let window = UIApplication.shared.keyWindow else {
      fatalError("No access to UIApplication Window")
    }
    let alertHeight = CGFloat(64 + 36 + window.safeAreaInsets.top)
    
    let alert = AlertBlackView(frame: CGRect(x: 0, y: -alertHeight, width: window.frame.width, height: alertHeight))
    alert.setup()
    alert.titleLabel.font = font
    alert.titleLabel.text = message
    window.addSubview(alert)
    // Animates the alert to show and disappear from the view
    UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut, .allowUserInteraction], animations: {
      alert.frame.origin.y = 0
      alert.alpha = 1.0
    }, completion: { _ in
      UIView.animate(withDuration: 0.3, delay: 1.4, options: [.curveEaseInOut, .allowUserInteraction], animations: {
        alert.frame.origin.y = -alertHeight
        alert.alpha = 0.0
      }, completion: { _ in
        alert.removeFromSuperview()
      })
    })
  }
  
  private func setup() {
    self.addSubview(popupView)
    self.addSubview(containerView)
    self.addSubview(titleLabel)
    NSLayoutConstraint.activate([
      popupView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 64),
      popupView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
      popupView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
      
      containerView.topAnchor.constraint(equalTo: popupView.topAnchor),
      containerView.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 0),
      containerView.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: 0),
      containerView.bottomAnchor.constraint(equalTo: popupView.bottomAnchor),
      
      titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16),
      titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16),
      titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
      titleLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -6),
      
    ])
  }
  
  lazy var popupView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor.fITCOGrey700.withAlphaComponent(0.93)
    view.layer.cornerRadius = 12
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layer.zeplinStyleShadows(color: .black, alpha: 0.06, x: 0, y: 15, blur: 24, spread: 0)
    return view
  }()
  lazy var containerView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.numberOfLines = 0
    label.textColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
}
