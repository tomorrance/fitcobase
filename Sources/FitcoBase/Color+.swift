//
//  Color+.swift
//  FitcoAlert
//
//  Created by Daisy on 2020/03/13.
//  Copyright © 2020 Daisy. All rights reserved.
//

import UIKit

extension UIColor {

  public class var tomatoRed: UIColor {
    return UIColor(red: 235.0 / 255.0, green: 24.0 / 255.0, blue: 8.0 / 255.0, alpha: 1.0)
  }

  public class var dim40: UIColor {
    return UIColor(red: 4.0 / 255.0, green: 4.0 / 255.0, blue: 15.0 / 255.0, alpha: 0.4)
  }

  public class var dim60: UIColor {
    return UIColor(red: 4.0 / 255.0, green: 4.0 / 255.0, blue: 15.0 / 255.0, alpha: 0.6)
  }

  public class var fITCOGrey50: UIColor {
//    return UIColor(red: 252.0 / 255.0, green: 252.0 / 255.0, blue: 1.0, alpha: 1.0)
    return UIColor(white: 1.0, alpha: 1.0)
  }

  public class var fITCOGrey100: UIColor {
    return UIColor(red: 247.0 / 255.0, green: 248.0 / 255.0, blue: 252.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey200: UIColor {
    return UIColor(red: 240.0 / 255.0, green: 243.0 / 255.0, blue: 247.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey300: UIColor {
    return UIColor(red: 212.0 / 255.0, green: 215.0 / 255.0, blue: 221.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey400: UIColor {
    return UIColor(red: 186.0 / 255.0, green: 188.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey500: UIColor {
    return UIColor(red: 143.0 / 255.0, green: 145.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey600: UIColor {
    return UIColor(red: 111.0 / 255.0, green: 115.0 / 255.0, blue: 127.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey700: UIColor {
    return UIColor(red: 81.0 / 255.0, green: 86.0 / 255.0, blue: 100.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey800: UIColor {
    return UIColor(red: 51.0 / 255.0, green: 55.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOGrey900: UIColor {
    return UIColor(red: 21.0 / 255.0, green: 28.0 / 255.0, blue: 47.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange50: UIColor {
    return UIColor(red: 1.0, green: 248.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange100: UIColor {
    return UIColor(red: 251.0 / 255.0, green: 210.0 / 255.0, blue: 195.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange200: UIColor {
    return UIColor(red: 249.0 / 255.0, green: 180.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange300: UIColor {
    return UIColor(red: 246.0 / 255.0, green: 150.0 / 255.0, blue: 116.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange400: UIColor {
    return UIColor(red: 244.0 / 255.0, green: 120.0 / 255.0, blue: 76.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange500: UIColor {
    return UIColor(red: 242.0 / 255.0, green: 91.0 / 255.0, blue: 37.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange600: UIColor {
    return UIColor(red: 220.0 / 255.0, green: 83.0 / 255.0, blue: 34.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange700: UIColor {
    return UIColor(red: 199.0 / 255.0, green: 75.0 / 255.0, blue: 31.0 / 255.0, alpha: 1.0)
  }

  public class var fITCOOrange800: UIColor {
    return UIColor(red: 177.0 / 255.0, green: 67.0 / 255.0, blue: 27.0 / 255.0, alpha: 0)
  }

  public class var fITCOOrange900: UIColor {
    return UIColor(red: 155.0 / 255.0, green: 58.0 / 255.0, blue: 24.0 / 255.0, alpha: 1.0)
  }
}
