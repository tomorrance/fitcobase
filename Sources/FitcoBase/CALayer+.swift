//
//  CALayer+.swift
//  FitcoAlert
//
//  Created by Daisy on 2020/03/13.
//  Copyright © 2020 Daisy. All rights reserved.
//
import UIKit

struct Shadow {
  let color: UIColor
  let alpha: Float
  let x: CGFloat
  let y: CGFloat
  let blur: CGFloat
}

extension CALayer {
  func shadow(_ shadow: Shadow) {
    shadowColor = shadow.color.cgColor
    shadowOpacity = shadow.alpha
    shadowOffset = CGSize(width: shadow.x, height: shadow.y)
    shadowRadius = shadow.blur / 2.0
  }
  
  func zeplinStyleShadows(
    color: UIColor = .black,
    alpha: Float = 0.5,
    x: CGFloat = 0,
    y: CGFloat = 2,
    blur: CGFloat = 4,
    spread: CGFloat = 0) {
    shadowColor = color.cgColor
    shadowOpacity = alpha
    shadowOffset = CGSize(width: x, height: y)
    shadowRadius = blur / 2.0
    if spread == 0 {
      shadowPath = nil
    } else {
      let dx = -spread
      let rect = bounds.insetBy(dx: dx, dy: dx)
      shadowPath = UIBezierPath(rect: rect).cgPath
    }
  }
}
