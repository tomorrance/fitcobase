//
//  NavBarButton.swift
//  
//
//  Created by Daisy on 2020/04/28.
//

import UIKit
import SnapKit

public class NavBarButton: UIButton {
  
  public override var isEnabled: Bool {
    didSet {
      if let enabled = enabledType, let unabled = unabledType {
        if isEnabled {
          if let font = enabled.font, let color = enabled.textColor {
            self.customLabel.font = font
            self.customLabel.textColor = color
            self.customLabel.text = enabled.text
          }
        } else {
          if let font = unabled.font, let color = unabled.textColor {
            self.customLabel.font = font
            self.customLabel.textColor = color
            self.customLabel.text = unabled.text
          }
        }
      }
    }
  }
  
  var enabledType: NavButtonType?
  var unabledType: NavButtonType?
  
  public func setLabel(enabledType: NavButtonType, unabledType: NavButtonType) {
    self.enabledType = enabledType
    self.unabledType = unabledType
  }
  
  public func setButton(image: UIImage) {
    self.customImageView.image = image
    
    self.heightConstraint?.constant = 24
    self.widthConstraint?.constant = 24

  }
  
  public func setButtonImage(image: UIImage, color: UIColor? = nil) {
    let tempImage = image.withRenderingMode(.alwaysTemplate)
    self.customImageView.image = tempImage
    if color != nil {
      self.customImageView.tintColor = color!
    } else {
      self.customImageView.tintColor = UIColor.fITCOGrey800
    }
    
    self.heightConstraint?.constant = 24
    self.widthConstraint?.constant = 24
    
  }
  
  public func setLabel(text: String, font: UIFont? = nil, color: UIColor? = nil) {
    self.customLabel.isHidden = false
    self.customLabel.text = text
    if color != nil {
      self.customLabel.textColor = color!
    } else {
      self.customLabel.textColor = UIColor.fITCOGrey800
    }
    
    if let font = font {
      self.customLabel.font = font
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initView()
  }
  
  var heightConstraint: NSLayoutConstraint?
  var widthConstraint: NSLayoutConstraint?
  
  private func initView() {
    containerView.isUserInteractionEnabled = false

    self.addSubview(containerView)
    containerView.addSubview(customImageView)
    containerView.addSubview(customLabel)
    
    containerView.snp.makeConstraints { (make) in
      make.top.leading.trailing.bottom.equalTo(self)
    }
    customImageView.snp.makeConstraints { (make) in
      make.leading.equalTo(containerView).offset(12)
      make.centerY.equalTo(containerView)
    }
    customLabel.snp.makeConstraints { (make) in
      make.top.bottom.equalTo(containerView)
      make.leading.equalTo(customImageView.snp.trailing)
      make.trailing.equalTo(containerView.snp.trailing).offset(-12)
    }
    
    heightConstraint = customImageView.heightAnchor.constraint(equalToConstant: 0)
    widthConstraint = customImageView.widthAnchor.constraint(equalToConstant: 0)
    
    heightConstraint?.isActive = true
    widthConstraint?.isActive = true
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  lazy var containerView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var customImageView: UIImageView = {
    let view = UIImageView()
    view.tintColor = UIColor.fITCOGrey800
    view.translatesAutoresizingMaskIntoConstraints = false
    view.contentMode = .scaleAspectFit
    return view
  }()
  
  lazy var customLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textColor = .fITCOOrange500
    return label
  }()
  
}
