//
//  NavBar.swift
//  
//
//  Created by Daisy on 2020/04/28.
//

import UIKit
import SnapKit

open class NavBar: UIView {
  
  var type: NavBarType
  
  public init(type: NavBarType) {
    self.type = type
    super.init(frame: .zero)
    initView()
    
    if let titleType = type.titleType {
      configure(titleType: titleType)
    }
    
    if let left = type.leftType {
      configure(leftType: left)
    }
    if let right = type.rightType {
      configure(rightType: right)
    }
  }
  
  required public init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  public func configure(titleType: NavTitleType) {
    if let font = titleType.font, let title = titleType.title {
      titleLabel.font = font
      self.setTitle(title)
    }
  }
  
  public func configure(leftType: NavButtonType) {
    if let image = leftType.image {
      leftButton.setButtonImage(image: image)
    }
    if let label = leftType.text {
      leftButton.setLabel(text: label, font: leftType.font)
    }
  }
  
  public func configure(rightType: NavButtonType) {
    if let image = rightType.image {
      rightButton.setButtonImage(image: image)
    }
    if let label = rightType.text {
      rightButton.setLabel(text: label, font: rightType.font)
    }
  }
  
  open func setTitle(_ title: String) {
    centerView.addArrangedSubview(titleWrapperView)
    titleWrapperView.addSubview(titleLabel)
    titleLabel.snp.makeConstraints { (make) in
      make.top.bottom.equalTo(titleWrapperView)
      make.center.equalTo(baseView)
    }
    titleLabel.text = title
  }
  
  
  // MARK: - InitView
  func initView() {
    self.addSubview(baseView)
    baseView.addSubview(leftButton)
    baseView.addSubview(centerView)
    baseView.addSubview(rightButton)
    self.addSubview(bottomDivider)
    baseView.snp.makeConstraints { (make) in
      make.top.bottom.equalTo(self)
      make.leading.equalTo(self)
      make.trailing.equalTo(self)
      make.height.equalTo(48)
    }
    leftButton.snp.makeConstraints { (make) in
      make.top.bottom.equalTo(baseView)
      make.leading.equalTo(baseView).offset(4)
    }
    centerView.snp.makeConstraints { (make) in
      make.top.bottom.equalTo(baseView)
      make.leading.equalTo(leftButton.snp.trailing)
      make.trailing.equalTo(rightButton.snp.leading)
    }
    rightButton.snp.makeConstraints { (make) in
      make.top.bottom.equalTo(baseView)
      make.trailing.equalTo(baseView).offset(-4)
    }
    bottomDivider.snp.makeConstraints { (make) in
      make.bottom.leading.trailing.equalTo(self)
      make.height.equalTo(0.5)
    }
  }
  
  lazy var titleWrapperView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  public lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textColor = .fITCOGrey800
    label.textAlignment = .center
    return label
  }()
  lazy var baseView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  public lazy var leftButton: NavBarButton = {
    let view = NavBarButton()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  public lazy var centerView: UIStackView = {
    let view = UIStackView()
    view.distribution = .fill
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  public lazy var rightButton: NavBarButton = {
    let view = NavBarButton()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  public lazy var bottomDivider: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = .fITCOGrey200
    return view
  }()
  
}
