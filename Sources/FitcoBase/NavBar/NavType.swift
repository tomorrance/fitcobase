//
//  File.swift
//  
//
//  Created by Daisy on 2020/04/28.
//

import UIKit

public struct NavTitleType {
  
  public init(title: String? = nil, font: UIFont? = nil) {
    self.title = title
    self.font = font
  }
  
  var title: String?
  public var font: UIFont?
  
}

public struct NavButtonType {
  public init(text: String? = nil, image: UIImage? = nil, font: UIFont? = nil, textColor: UIColor? = nil) {
    self.text = text
    self.image = image
    self.font = font
    self.textColor = textColor
  }
  
  var text: String?
  var image: UIImage?
  public var font: UIFont?
  var textColor: UIColor?

}

public struct NavBarType {
  
  public init(titleType: NavTitleType?, leftType: NavButtonType?, rightType: NavButtonType?) {
    self.titleType = titleType
    self.leftType = leftType
    self.rightType = rightType
  }
  
  public var titleType: NavTitleType?
  public var leftType: NavButtonType?
  public var rightType: NavButtonType?
}
