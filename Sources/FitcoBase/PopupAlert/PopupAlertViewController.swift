//
//  PopupAlertViewController.swift
//  
//
//  Created by Daisy on 2020/04/28.
//

import UIKit
import SnapKit

public struct PopupDataType {
  public init(title: String, leftText: String, rightText: String, leftColor: UIColor, rightColor: UIColor, titleFont: UIFont, leftFont: UIFont, rightFont: UIFont) {
    self.title = title
    self.leftText = leftText
    self.rightText = rightText
    self.leftColor = leftColor
    self.rightColor = rightColor
    self.titleFont = titleFont
    self.leftFont = leftFont
    self.rightFont = rightFont
  }
  let titleFont: UIFont
  let title: String
  let leftText: String
  let rightText: String
  let leftColor: UIColor
  let rightColor: UIColor
  let leftFont: UIFont
  let rightFont: UIFont
}

public class PopupAlertViewController: UIViewController {
  
  deinit {
    debugPrint("PopupTwoButtonViewController deinit")
  }
  public init(dataType: PopupDataType) {
    super.init(nibName: nil, bundle: nil)
    self.configure(type: dataType)
    self.modalPresentationStyle = .overFullScreen
    self.modalTransitionStyle = .crossDissolve
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    initView()
  }
  
  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  func initView() {
    
    view.backgroundColor = UIColor.dim60
    
    view.addSubview(popupView)
    popupView.addSubview(titleView)
    popupView.addSubview(lineView)
    popupView.addSubview(buttonStack)
    
    titleView.addSubview(titleLabel)
    buttonStack.addArrangedSubview(cancelButton)
    buttonStack.addArrangedSubview(confirmButton)
    
    NSLayoutConstraint.activate([
      popupView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
      popupView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      popupView.widthAnchor.constraint(equalToConstant: 280),
      popupView.heightAnchor.constraint(equalToConstant: 128),
      
      titleView.topAnchor.constraint(equalTo: popupView.topAnchor),
      titleView.leadingAnchor.constraint(equalTo: popupView.leadingAnchor),
      titleView.trailingAnchor.constraint(equalTo: popupView.trailingAnchor),
      
      //      titleLabel.topAnchor.constraint(equalTo: titleView.topAnchor, constant: 36),
      titleLabel.centerXAnchor.constraint(equalTo: titleView.centerXAnchor),
      titleLabel.centerYAnchor.constraint(equalTo: titleView.centerYAnchor),
      titleLabel.leftAnchor.constraint(greaterThanOrEqualTo: titleView.leftAnchor),
      titleLabel.rightAnchor.constraint(lessThanOrEqualTo: titleView.rightAnchor),
      
      lineView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
      lineView.leadingAnchor.constraint(equalTo: popupView.leadingAnchor),
      lineView.trailingAnchor.constraint(equalTo: popupView.trailingAnchor),
      lineView.heightAnchor.constraint(equalToConstant: 1),
      
      buttonStack.topAnchor.constraint(equalTo: lineView.bottomAnchor),
      buttonStack.leadingAnchor.constraint(equalTo: popupView.leadingAnchor),
      buttonStack.trailingAnchor.constraint(equalTo: popupView.trailingAnchor),
      buttonStack.heightAnchor.constraint(equalToConstant: 40),
      buttonStack.bottomAnchor.constraint(equalTo: popupView.bottomAnchor)
    ])
    
  }
  lazy var popupView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = .white
    view.layer.cornerRadius = 12
    view.layer.zeplinStyleShadows(color: .black, alpha: 0.16, x: 0, y: 24, blur: 36, spread: 0)
    return view
  }()
  lazy var titleView: UIView = {
    let view = UIView()
    view.layer.cornerRadius = 12
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = .white
    return view
  }()
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textColor = UIColor.fITCOGrey800
    return label
  }()
  lazy var lineView: UIView = {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = .fITCOGrey200
    return view
  }()
  lazy var buttonStack: UIStackView = {
    let stack = UIStackView()
    stack.translatesAutoresizingMaskIntoConstraints = false
    stack.axis = .horizontal
    stack.distribution = UIStackView.Distribution.fillEqually
    return stack
  }()
  public lazy var cancelButton: UIButton = {
    let button = UIButton()
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitleColor(.fITCOGrey400, for: .normal)
    return button
  }()
  public lazy var confirmButton: UIButton = {
    let button = UIButton()
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitleColor(.fITCOOrange500, for: .normal)
    return button
  }()
  
  func configure(type: PopupDataType) {
    titleLabel.text = type.title
    titleLabel.font = type.titleFont
    setLeftTitle(title: type.leftText)
    setRightTitle(title: type.rightText)
    setLeftLabelColor(color: type.leftColor, font: type.leftFont)
    setRightLabelColor(color: type.rightColor, font: type.rightFont)
  }
  private func setLeftTitle(title: String) {
    cancelButton.setTitle(title, for: .normal)
  }
  private func setRightTitle(title: String) {
    confirmButton.setTitle(title, for: .normal)
  }
  private func setLeftLabelColor(color: UIColor, font: UIFont) {
    cancelButton.setTitleColor(color, for: .normal)
    cancelButton.titleLabel?.font = font
  }
  private func setRightLabelColor(color: UIColor, font: UIFont) {
    confirmButton.setTitleColor(color, for: .normal)
    confirmButton.titleLabel?.font = font
  }
  
}
